#!/bin/bash

# Remove Installation Folder
rm -rf /var/www/html/install

## Unzip Modules
unzip /etc/assets/modules/v3.2.11-packlink.zip -d /var/www/html/modules
unzip /etc/assets/modules/v3.5.6-ps_metrics.zip -d /var/www/html/modules
unzip /etc/assets/modules/v5.8.0-paypal.zip -d /var/www/html/modules
unzip /etc/assets/modules/v6.1.7-ps_accounts.zip -d /var/www/html/modules
unzip /etc/assets/modules/v8.3.1.0-ps_checkout.zip -d /var/www/html/modules
rm /etc/assets/modules/v3.2.11-packlink.zip
rm /etc/assets/modules/v3.5.6-ps_metrics.zip
rm /etc/assets/modules/v5.8.0-paypal.zip
rm /etc/assets/modules/v6.1.7-ps_accounts.zip
rm /etc/assets/modules/v8.3.1.0-ps_checkout.zip

## Moving Themes
mv /etc/assets/themes/v1.0.7-digital17.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.7-furniture.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.7-revostore.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.7-shopnow.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.7-ebusiness.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.8-amazonas.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.8-foodstore.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.9-freshfood.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.9-giftshop.zip /var/www/html/themes
mv /etc/assets/themes/v1.0.9-probusiness.zip /var/www/html/themes
mv /etc/assets/themes/v1.1.0-jewelrystore.zip /var/www/html/themes
mv /etc/assets/themes/v1.1.1-kidshop.zip /var/www/html/themes
mv /etc/assets/themes/v1.1.2-boom17.zip /var/www/html/themes
mv /etc/assets/themes/v2.0.7-cosmetic17.zip /var/www/html/themes
mv /etc/assets/themes/v2.0.9-thecosmetic.zip /var/www/html/themes