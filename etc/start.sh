#!/bin/bash

upload_max_filesize=32M
memory_limit=128M

for key in upload_max_filesize post_max_size max_execution_time max_input_time
do
 sed -i "s/^\($key\).*/\1 $(eval echo = \${$key})/" /etc/php/7.4/cli/php.ini
done

/usr/sbin/sshd -D &

apache2ctl -D FOREGROUND