<div align="center">

![](/assets/images/logo_compressed.png "docker-prestashop")

<br>

![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white) ![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white) ![PHP](https://img.shields.io/badge/php-%23777BB4.svg?style=for-the-badge&logo=php&logoColor=white) 	![Apache](https://img.shields.io/badge/apache-%23D42029.svg?style=for-the-badge&logo=apache&logoColor=white) ![MySQL](https://img.shields.io/badge/mysql-%2300f.svg?style=for-the-badge&logo=mysql&logoColor=white)

<br>
<a href="#"><img src="https://img.shields.io/badge/ubuntu-20.04-red"></a>
<a href="#"><img src="https://img.shields.io/badge/php-7.4-red"></a>
<a href="#"><img src="https://img.shields.io/badge/prestashop-8.0.4-red"></a>
<a href="https://paypal.me/rodrigoodhin"><img src="https://img.shields.io/badge/donate-PayPal-blue"></a>
<br><br>
</div>

# Docker + Prestashop

Development environment for Prestashop

&nbsp;
&nbsp;
&nbsp;

## Download image from Docker Hub

```shell
docker pull rodrigoodhin/prestashop
```

&nbsp;
&nbsp;
&nbsp;

## Create a new image

```shell
docker build -t <YOUR_DOCKER_HUB_USER>/prestashop . 
```

&nbsp;
&nbsp;
&nbsp;

## Push created image to docker hub

```shell
docker login
docker push <YOUR_DOCKER_HUB_USER>/prestashop
```

&nbsp;
&nbsp;
&nbsp;

## Using docker-compose.yml

*To use this image within your project, copy the file `docker-compose.yml` to your project's folder and execute the steps below.*

### Start services

```shell
docker-compose up -d
```

### List containers to get SSH port

```shell
docker container ls
```

### Open SSH connection

- *The password is: app*

```shell
ssh app@localhost -p <SSH_PORT>
```

### Remove Istallation Folder and Install Modules and Themes
```
docker exec prestashop_app /etc/installAssets.sh
```