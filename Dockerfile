FROM ubuntu:20.04
SHELL ["/bin/bash", "-c"]
LABEL maintainer="Rodrigo Odhin"

ARG DEBIAN_FRONTEND=noninteractive

# Install system libs
RUN apt-get update --fix-missing
RUN apt-get install -y openssh-server vim curl git sudo unzip

# Install Apache and PHP 7.4
RUN apt install -y apache2 apache2-utils php7.4 libapache2-mod-php libapache2-mod-wsgi
RUN apt install -y php7.4-cli php7.4-json php7.4-common php7.4-mysql php7.4-zip php7.4-gd php7.4-mbstring php7.4-curl php7.4-xml php7.4-bcmath php7.4-intl
RUN a2enmod rewrite

# Change PHP config
ENV PHP_UPLOAD_MAX_FILESIZE 50000000
ENV PHP_POST_MAX_SIZE 1000000000
ENV PHP_MEM_LIMIT 1000000000
RUN echo "post_max_size = 1024M" >> /etc/php/7.4/apache2/php.ini
RUN echo "upload_max_filesize = 50M" >> /etc/php/7.4/apache2/php.ini

# Prepare Prestashop
RUN cd /var/www/html && rm -f index*
COPY etc/prestashop_8.0.4 /var/www/html
RUN chown -R www-data:www-data /var/www/html

# Configure the Apache Web Server
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

RUN mkdir /var/run/sshd

RUN echo 'root:root' |chpasswd
RUN sed -ri 's/^PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -ri 's/UsePAM yes/#UsePAM yes/g' /etc/ssh/sshd_config
RUN echo 'Banner /etc/banner' >> /etc/ssh/sshd_config

COPY etc/banner /etc/

# Prepare Assets
COPY assets/modules /etc/assets/modules
COPY assets/themes /etc/assets/themes
COPY etc/installAssets.sh /etc/

RUN useradd -ms /bin/bash app
RUN adduser app sudo
RUN echo 'app:app' |chpasswd

EXPOSE 80
EXPOSE 22

RUN mkdir /projects
VOLUME /projects

ADD etc/start.sh /
RUN ["chmod", "777", "/start.sh"]
ENTRYPOINT ["/start.sh"]
